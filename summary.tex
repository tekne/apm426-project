\documentclass{article}
%\usepackage[margin=1in]{geometry}

\usepackage[backend=biber]{biblatex}
\usepackage{apm426}
\usepackage{hyperref}
\hypersetup{
  colorlinks,
  linkcolor={red!50!black},
  citecolor={blue!50!black},
  urlcolor={blue!80!black}
}

\addbibresource{references.bib}

\title{Constructing Solutions of the Vacuum Einstein Constraint Equations with Special Asymptotics}
\author{Jad Elkhaleq Ghalayini}

\begin{document}

\maketitle

The application of ``gluing", a technique from geometric analysis \cites{gluing}{pollack}, to general relativity has yielded several valuable insights into the behavior of the Einstein constraint equations. In this paper, we will summarize the application of gluing techniques in \cite{scalar_gluing} and \cite{vacuum_asymptotics} to the construction of scalar-flat solutions to the vacuum Einstein constraint equations with special asymptotics. Following \cite{scalar_gluing}, we will describe how we can construct asymptotically-flat, scalar-flat solutions that are rotationally symmetric at infinity and which extend specified time-symmetric initial data on a compact set. This, in particular, will imply that metrics symmetric at infinity are, in some sense, generic among scalar-flat, asymptotically-flat metrics \cite{scalar_gluing}. We will then follow \cite{vacuum_asymptotics} to generalize these results to the construction of solutions extending (not necessarily time-symmetric) initial data to slices of Kerr spacetime, leading to an argument that this class of solutions is dense in a suitably weighted Sobolev space.

The basic object of study in general relativity is a Lorentzian manifold \((\mc{S}, \ol{g})\) satisfying \textbf{Einstein's equation}
\begin{equation}
 \Ric(\ol{g}) - \frac{1}{2}R(\ol{g})\ol{g} = 8\pi T
 \label{equation:einstein}
\end{equation}
where \(T\) is the stress-energy tensor of matter \cite{scalar_gluing}. In this summary we consider the \textbf{vacuum Einstein constraint equations}, in which we assume \(T = 0\). Taking the trace \cite{scalar_gluing} then allows us to simplify equation \ref{equation:einstein} to
\begin{equation}
  \Ric(\ol{g}) - \frac{1}{2}R(\ol{g}) = 0 \iff \Ric(\ol{g}) = 0
  \label{equation:vacuum}
\end{equation}
The first solution to equation \ref{equation:einstein}, a solution of \ref{equation:vacuum}, was obtained by Schwarzschild with the metric \cite{wald}
\begin{equation}
 \ol{g} = -\left(1 - \frac{2m}{r}\right)dt^2
   + \left(1 - \frac{2m}{r}\right)^{-1}dr^2
   + r^2g_\Omega,
 \quad g_\Omega = (d\theta^2 + \sin^2\theta d\phi^2)
\end{equation}
in standard spherical coordinates \((t, r, \theta, \phi)\) for \(\reals \times \reals^3\). The singularity at \(r = 2m\) is merely a coordinate singularity, and the metric can be extended to all of \(\mc{S} = \reals \times (\reals^3 \setminus \{0\})\) \cites{wald}{scalar_gluing}. These solutions are parametrized by a ``mass" \(m > 0\), corresponding to that of the black hole at spacelike coordinate \(0\), which is a true singularity \cites{wald}{vacuum_asymptotics}.

Any tractable physical theory must support some notion of an ``ideally isolated system:" as in classical mechanics, modeling the orbit of a single planet to reasonable accuracy should not require that we keep track of every object in the local supercluster. We should, in effect, be able to ``ignore the rest of the universe." Attempting to translate this notion to general relativity, we note that, if we want to study a system while ignoring the effects of distant matter and cosmological curvature, we'd want to consider the system ``on its own in otherwise flat space-time," the system's impact on the deflection of space-time from flatness decreasing as our distance from it increased. From this intuition, we yield a naive notion of \textbf{asymptotic flatness}: a space-time which decays to flat (Minkowski) space-time as we move out to infinity \cites{wald}{nlab:asymptotically_flat_spacetime}.

A flaw in this intuition is that, in general relativity, we do not have a flat background metric \(\ol{\eta}\) to allow us to define the falloff rates of the curvature of the space-time metric \(\ol{g}\). In particular, we do not have a preferred radial coordinate \(r\) telling us ``how far we've gone out" with which to define falloff rates \cite{wald}. One way to overcome this issue is to define a space-time to be asymptotically flat if there are \textit{any} coordinates \(x^0, x^1, x^2, x^3\) such that \(\ol{g}_{\mu\nu} \in \ol{\eta}_{\mu\nu} + O(1/r)\), where \(r = \sqrt{(x^1)^2 + (x^2)^2 + (x^3)^2}\).  Using this definition, it is clear to see that the Schwarzschild solutions satisfy this criterion using standard spherical coordinates to represent points \((t, x, y, z)\), as we have \cite{wald}
\begin{equation}
	\ol{\eta} = -dt^2 + dr^2 + r^2g_\Omega
	\implies \ol{g} = \ol{\eta} + \frac{2m}{r}dt^2 - \frac{2m}{r - 2m}dr^2
	 \in \ol{\eta} + \mc{O}(1/r)
\end{equation}
Though easy to grasp, this definition is exceedingly difficult to work with, so another one by Ashtekar and Hansen is used in practice \cite{wald}. We will not get into the details of this definition, but we will later provide a more precise definition of asymptotic flatness for Riemannian manifolds, which we will show serve as initial data to the Einstein contraint equations, with asymptotically flat initial data corresponding to asymptotically flat solutions.

While the Einstein constraint equations do not describe the ``evolution" of space-time, instead describing constraints on the possibilities for a picture of the whole of space-time, it is possible to reformulate them as an initial value problem \cites{scalar_gluing}{vacuum_asymptotics}. In the vacuum case, the initial data consists of an oriented 3-manifold \(\mc{M}\), a Riemannian metric \(g\) and a symmetric \((0, 2)\)-tensor \(K\) on \(\mc{M}\). The Gauss and Codazzi equations tell us the conditions under which \(g\) and \(K\) are, respectively, the induced metric and second fundamental form of \(\mc{M}\) embedded inside of some Ricci-flat spacetime \((\mc{S}, \ol{g})\), being \cite{wald}
\begin{equation}
  \begin{array}{rl}
  R(g) - |K|^2 + H^2 &= 0 \\
  \Div_g(K) - dH &= 0
  \end{array}
  \quad \text{where} \
    H = \Tr_g(K) = g^{ij}K_{ij}, (\Div_g(K))_i = g^{jk}K_{ij; k}
  \label{equation:einstein_initial}
\end{equation}
The notion of asmyptotic flatness translates naturally to this formulation: namely, we say that initial data is asymptotically flat if there exist coordinates \(x^1, x^2, x^3\) such that \(g_{\mu\nu} = \delta_{\mu\nu} + \mc{O}(1/r)\), with \(r\) defined as above and \(\delta\) the Kronecker delta. That said, to properly state the results of \cites{scalar_gluing}{vacuum_asymptotics}, it helps to have a more precise definition of asymptotic flatness which introduces some related terminology.

Following \cite{scalar_gluing}, we say a complete Riemannian manifold \((\mc{M}, g)\) is \textbf{asymptotically flat} if there exists a compact set \(C \subset \mc{M}\) such that \(\mc{M} \setminus C\) can be written as a finite union, where \(B_1\) denotes a unit ball,
\begin{equation}
	\mc{M} \setminus C = \bigcup_{i = 1}^kN_i, \qquad \Phi_i: N_i \to \reals^n \setminus B_1
\end{equation}
of \textbf{ends} \(N_i\) where each \(\Phi_i\) is a diffeomorphism and each tensor \((\Phi_{i*}g - \delta)\) decays suitably, for example
\begin{equation}
	(\Phi_{i*}g - \delta)(x) \in \mc{O}_2(|x|^{-p}), \qquad R(g)(x) \in \mc{O}(|x|^{-q}), \qquad q > n, \qquad p > (n - 2)/2
\end{equation}
Note we write that \(f \in \mc{O}_k(g)\) if, for all multi-indices \(|\alpha| \leq k\), \(D_\alpha f\) exists and is in \(\mc{O}(g)\). Intuitively, \((\mc{M}, g)\) is asymptotically flat if, except for a compact set \(C\), it ``looks like" copies of \(\reals^n\) on which the pushforward of the metric \(g\) decays as in the naive definition. We will call initial data \((\mc{M}, g, K)\) \textbf{asymptotically flat} if \((\mc{M}, g)\) satisfies the above definition for \(n = 3\).
For simplicity, we will consider initial data with only one end \(N\); however, the results we develop can be generalized directly to manifolds with multiple ends \cite{vacuum_asymptotics}. We will also assume some reasonable conditions on \(C\), for example, that the boundary \(\partial C\) is smooth.
Alternatively, we can introduce the \textbf{momentum tensor} \cite{vacuum_asymptotics}
\begin{equation}
  \pi^{ij} = K^{ij} - \Tr_g(K)g^{ij} = K^{ij} - Hg^{ij}
\end{equation}
in which case the constraints in Equation \ref{equation:einstein_initial} take the form \cite{vacuum_asymptotics}
\begin{equation}
  \Phi(g, \pi) = (\mc{H}(g, \pi), \Div_g\pi) = 0, \qquad \mc{H}(g, \pi) = R(g) + \frac{1}{2}(\Tr_g\pi)^2 - |\pi|^2
  \label{equation:momentum_einstein}
\end{equation}
This yields an alternative representation of initial data \((\mc{M}, g, K)\) as the tuple \((\mc{M}, g, \pi)\).

We define initial data to be \textbf{time symmetric} if it additionally satisfies the condition \(K = 0\): in this case, as we have \(H = \Tr 0 = 0\), the constraints in Equation \ref{equation:einstein_initial} simplify to \(R(g) = 0\), i.e. \((\mc{M}, g, 0)\) is initial data for the Einstein constraint equations if and only if \((\mc{M}, g)\) is scalar-flat.

The Schwarzschild solutions turn out to be the only \textbf{scalar flat} (with scalar curvature \(R(\ol{g}) = 0\)) solutions of the Einstein constraint equations which are rotationally symmetric throughout space \cite{scalar_gluing}. Equivalently, the initial data for the Schwarzschild solutions, are the only time-symmetric, rotationally-symmetric initial data for the vacuum Einstein contraint equations. Generalizing this concept, it makes sense to ask if a given solution is rotationally symmetric ``at (space-like) infinity," that is, outside a compact set at each time-slice. We call such a solution \textbf{Schwarzschild}. Again, equivalently, we ask whether the initial data for such a solution is rotationally symmetric ``at infinity", i.e. outside a compact set, calling such initial data \textbf{Schwarzschild}.

On the other hand, we say that a Riemannian manifold \((\mc{M}, g)\) is \textbf{conformally flat} if there exists an open cover \(\{U_i \to X\}_{i \in I}\) of \(\mc{M}\) and on each \(U_i \subset \reals^3\) a smooth function \(f_i\) such that \(R(e^{f_i}g|_{U_i})\), i.e. it is locally taken to a flat manifold by a conformal transformation
\cite{nlab:conformally_flat_manifold}. Similarly to the case of Schwarzschild solutions, we say a Riemannian manifold is \textbf{conformally flat} ``at infinity" if it is conformally flat outside a compact set \cite{scalar_gluing}.

With the above definitions in hand, we can precisely state the problem solved in \cite{scalar_gluing}: restricting our attention to asymptotically-flat, conformally-flat, time-symmetric initial data, can we construct a Schwarzschild solution which agrees with this initial data on an arbitrary compact set? Our goal is to show that we can indeed do so: that is, given asymptotically flat initial data \((\mc{M}, g, 0)\) where \(\mc{M}\) is the union of compact \(C\) and an end \(N\) (with diffeomorphism \(\Phi: N \to \reals^3 \setminus B_1\)), we can construct initial data \((\mc{M}, g', 0)\) such that \(g_0\) agrees with \(g\) on \(C\) and the pushforward \(\Phi_*g'\) agrees with the (restriction of the) Schwarzschild metric on \(\reals^3 \setminus \{0\}\) outside a ball of large radius in \(\reals^3 \setminus B_1\) \cite{vacuum_asymptotics}.

We begin with the slightly simpler result proved as Theorem 2 in \cite{scalar_gluing}: given asymptotically-flat initial data \((\reals^3, g, 0)\), i.e. an asymptotically-flat, scalar-flat Riemannian metric \(g_0\) on \(\reals^3\) (the paper actually proves the result in \(\reals^n\)), and a compact set \(C \subset \reals^3\), we can construct a metric \(g_0\) which agrees with the Schwarzschild metric (for a given mass) outside a large ball in \(\reals^3\), and agrees with \(g\) on \(C\). In brief, we perform this construction by first gluing a time-symmetric asymptotically-flat metric to a Schwarzschild at a ``large" radius \(R\), with the gluing occuring in an annular region in which the scalar curvature is near zero, decaying like a power of \(R^{-1}\). We now have a metric which agrees with \(g\) on \(C\) and is spherically symmetric: our goal from here is to deform it to render the scalar curvature zero without jeopardizing these properties. This is done using a technique of local scalar curvature deformation in a compact domain of a Riemannian manifold \((\mc{M}, g)\) developed in Section 3 of the paper.

While might have expected rotational symmetry at infinity to impose a rather strong constraint on the behavior of such solutions. It is easy to show, for example, that we cannot construct a conformally-flat, asymptotically-flat metric on \(\reals^3\) which is symmetric at infinity and scalar-flat \cite{scalar_gluing}. It turns out, however, that to the contrary, symmetric solutions are in a sense \textit{generic} among scalar-flat, asymptotically-flat solutions to the Einstein constraint equations \cite{scalar_gluing}. Specifically, Theorem 7 in \cite{scalar_gluing} tells us that for any asymptotically flat \((\mc{M}, g)\) with nonnegative (resp. zero) scalar curvature, given any \(\epsilon > 0\), we can find a metric \(g_0\) with nonnegative (resp. zero) scalar curvature which, outside a compact set, is conformally flat and scalar-flat, and satisfies the \(\epsilon\)-\textbf{quasi-isometry} condition on \(\mc{M}\); that is, for every vector \(v\) in the tangent bundle of \(\mc{M}\),
\begin{equation}
1 - \epsilon \leq \frac{g_0(v, v)}{g(v, v)} \leq 1 + \epsilon
\end{equation}
Directly applying this theorem, we obtain that for any \(\epsilon\), every Schwarzschild metric, scalar-flat can be \(\epsilon\)-quasi-isometrically approximated a metric which is scalar-flat and conformally-flat at infinity, and in this sense the construction in Theorem 2 can be said to be generic \cite{scalar_gluing}.

We now wish to generalize the above to the more general problem of initial data \((\mc{M}, g, 0)\). Following \cite{vacuum_asymptotics}, it turns out we can even further generalize the above results to asymptotically-flat vacuum initial data \((\mc{M}, g, K)\) that is not necessarily time-symmetric or conformally flat, though we have to loosen the requirement that the constructed metric is Schwarzschild. We begin by introducing the \textbf{Kerr solution} as a generalization of the Schwarzschild solution. Whereas physically we can consider the Schwarzschild solution to be the description of a stationary black hole at the origin, parametrized by the mass \(m\) of the black hole (or equivalently the Schwarzschild radius \(r_s = 2m\), corresponding to the location of the event horizon), the Kerr solution describes a \textit{rotating} black hole.

The Kerr solution is a family of solutions with ten parameters: the energy-momentum \((E, \mb{P}) \in \reals \times \reals^3\), the angular momentum \(\mb{J} \in \reals^3\) at spacelike infinity, and a quantity \(\mb{C} \in \reals^3\) related to the center of mass. In the case where \(g\) is asymptotically flat, conformally flat near infinity, and has zero scalar curvature, we have \(\mb{C} \propto m\mb{c}_0\) where \(m\) is the mass of the black hole and \(\mb{c}_0\) is its translation from the origin \cite{vacuum_asymptotics}.
We call a pair \((\mc{M} = \reals^3, g)\) which is initial data for a member of the family of Kerr solutions a \textbf{Kerr slice}.

The value of the parameters \((E, \mb{P}, \mb{J}, \mb{C})\) for a Kerr slice turn out to be determined by limits of integrals over Euclidean spheres, namely:
\begin{align}
  E &= \frac{1}{16\pi}\lim_{R \to \infty}\oint_{|x| = R}\sum_{i, j}(g_{ij, i} - g_{ii, j})\nu^jd\sigma_g \label{equation:energy} \\
  P_i &= \frac{1}{8\pi}\lim_{R \to \infty}\oint_{|x| = R}\sum_j\pi_{ij}\nu^jd\sigma_g \label{equation:momentum} \\
  J_i &= \frac{1}{8\pi}\lim_{R \to \infty}\oint_{|x| = R}\sum_{j, k}\pi_{jk}Y_i^j\nu^kd\sigma_g \label{equation:angular} \\
  C_k &= \lim_{R \to \infty}\oint_{|x| = R}\sum_{i, j}x^k(g_{ij,i} - g_{ii, j})\nu^jd\sigma_g - \lim_{R \to \infty}\oint_{|x| = R}\sum_i(g_{ik}\nu^i = g_{ii}\nu^k)d\sigma_g \label{equation:com}
\end{align}
For an arbitrary asymptotically flat solution, we can prescribe the following asymptotic conditions on which \ref{equation:energy}, \ref{equation:momentum}, \ref{equation:angular}, \ref{equation:com} converge:
\begin{equation}
\begin{array}{rr}
g_{ij}(x) \in \delta_{ij} + \mc{O}(1/|x|) & K_{ij}(x) \in \mc{O}(1/|x|^2) \\
g_{ij}(x) - g_{ij}(-x) \in \mc{O}(1/|x|^2) & K_{ij}(x) + K_{ij}(-x) \in \mc{O}(1/|x|^3) \\
g_{ij, k}(x) + g_{ij, k}(-x) \in \mc{O}(1/|x|^3) & K_{ij,k}(x) - K_{ij,k}(-x) \in \mc{O}(1/|x|^4)
\end{array}
\label{equation:ac}
\end{equation}
\cite{vacuum_asymptotics} calls the conditions in Equattion \ref{equation:ac} (AC), requiring in addition analgous conditions on successive derivatives as needed.

Similarly to the situation of Schwarzschild solutions among time-symmetric asymptotically flat solutions described above, Section 3 of \cite{vacuum_asymptotics} is dedicated to showing that the set of solutions satisfying (AC) are dense in the set of all asymptotically flat solutions under a suitable weighted Sobolev norm. Recall that for open \(\Omega \subset \reals^d\), \(p \in [1, \infty]\) and \(s \in \mbb{N}\), we define the \textbf{Sobolev space} \cite{sobolev}
\begin{equation}
W^{k, p}(\Omega) = \{f \in L^p(\Omega) : \forall |\alpha = (\alpha_1,...,\alpha_n)| \leq k, D^\alpha_xf \in L^p(\Omega)\}
\end{equation}
i.e. the space of all \(p\)-Lebesgue functions \(f\) on \(\Omega\) whose partial derivatives up to the \(n^{th}\) order are all defined and \(p\)-Lebesgue. We can define a norm on this space as follows:
\begin{equation}
  |f|_{k, p} = \left(\sum_{|\alpha| \leq k}\int_\Omega|D^\alpha f|^p\right)^{1/p}
\end{equation}
where \(\alpha\) ranges over multi-indices.
Generalizing this, we will define the \textbf{weighted Sobolev space} \(W^{k, p}_{-\delta}(M, g)\) to be the space of functions on \(\mc{M}\) for which the assigned \textbf{weighted Sobolev norm}
\begin{equation}
  |f|_{k, p, -\delta} = \sum_{0 \leq |\alpha| \leq k}\left(
    \int_M(|D^\alpha f|\rho^{\delta + |\alpha|})^p\rho^{-3}d\mu_g
  \right)^{1/p}
\end{equation}
is well-defined, where \(\rho\) is some function which equals \(|x|\) near infinity in the end of \(\mc{M}\) and \(\alpha\) ranges over multi-indices \cite{vacuum_asymptotics}. This Sobolev space is strong enough to have continuous and well-defined energy and momentum, but angular momentum is discontinuous and may not even be well-defined \cite{vacuum_asymptotics}.

With this definition, given an asymptotically flat Riemannian manifold \((\mc{M}, g)\), the first density theorem in \cite{vacuum_asymptotics}, Theorem 1, tells us that vacuum initial data sets which satisfy Equation \ref{equation:ac} is dense in the weighted Sobolev space of vacuum initial data \((\mc{M}, g_{ij} - \delta_{ij}, \pi_{ij})\) where we take
\begin{equation}
  g_{ij} - \delta_{ij} \in W^{2, p}_{-\delta}(\mc{M}, g), \qquad \pi_{ij} \in W^{1, p}_{-\delta}(\mc{M}, g), \quad \delta \in (1/2, 1), \quad p > 3/2
\end{equation}
that is, we can find a vaccum initial data set \((\bar{g}, \bar{\pi})\) which is within \(\epsilon\) of \((g, \pi)\) in the \(W^{2, p}_{-\delta}(\mc{M}, g) \times W^{1, p}_{-\delta}(\mc{M}, g)\) norm and satisfies (AC), with mass and linear momentum within \(\epsilon\) of those of \((\mc{M}, g, \pi)\).

Theorem 4 in \cite{vacuum_asymptotics} tells us that, given a manifold \(\mc{M}\), a compact set \(C \subset \mc{M}\) and arbitrary asymptotically-flat initial data \((\mc{M}, g, \pi)\) (note here we specify the momentum tensor \(\pi\) instead of the second fundamental form \(K\)), we can construct initial data \((\mc{M}, g_0, \pi_0)\) which agrees with the initial data in \(C\) and is identical to that of a particular suitable Kerr slice outside a large ball in the end \(N\) of \(\mc{M}\). That is, \(g_0|_C = g|_C\), and if \(\Phi: N \to \reals^3\) is the chart for the end of \(N\), then there exists some
radius \(R\) such that \((\Phi_*g_0)|_{B_R}\) is equal to the desired Kerr slice.

This construction is achieved as follows, in Sections 4 and 5 of \cite{vacuum_asymptotics}: given asymptotically flat initial data \((g, \pi)\) on \(\mc{M}\) which satisfies Equation \ref{equation:ac}, we choose a sufficiently large radius \(R\). In the annulus from \(R\) to \(2R\) (the ``gluing region"), we can use gluing to smoothly patch the given metric and second fundamental form to the metric and second fundamental form coming from a Kerr slice. This produces an approximate solution of the vacuum constraints: we can alter this solution with a smooth perturbation compactly supported within the closed gluing region to data \((g_0, \pi_0)\).

Taking the fuction \(\Phi\) defined in Equation \ref{equation:momentum_einstein} and writing the Einstein constraint equations as \(\Phi(g, \pi) = 0\), the paper makes the key observation that the constraint function \(\Phi(g_0, \pi_0)\) can be shown to lie in a ten-dimensional real vector space: this gives a ten-dimensional \textbf{obstruction space}. As we have ten degrees of freedom afforded in choosing the parameters \((E, \mb{P}, \mb{J}, \mb{C})\) of the Kerr slice we wish to glue to, we can construct a map between this ten-dimensional parameter space and the ten-dimensional obstruction space opf nonzero degree. This is used to set \((g_0, \pi_0)\) to 0, giving a solution as desired.

It turns out that this result can be somewhat generalized in that the full statement of Theorem 4 actually applies to a whole class of ``admissible families" \cite{vacuum_asymptotics}. Specifically, it turns out that, other than the Kerr slices, the families of solutions to which this gluing technique can be applied turn out to be exactly the solutions which allow effectively attaining values \((E, \mb{P}, \mb{J}, \mb{C})\) in the parameter space \cite{vacuum_asymptotics}.

The paper \cite{vacuum_asymptotics} then goes on to combine the first density theorem (Theorem 1) with the gluing construction in Theorem 4 to prove a second density theorem, Theorem 5. Namely, for any asymptotically flat solution \((\mc{M}, g, \pi)\) of the vacuum constraints, given any \(\epsilon > 0\), we can find a solution \((g_0, \pi_0)\) within \(\epsilon\) of \((g, \pi)\) in the weighted Sobolev space described above with ADM energy-momentum \((E, \mb{P})\) (as defined in equations \ref{equation:energy}, \ref{equation:momentum})
within \(\epsilon\) of \((g, \pi)\)'s, such that, as in Theorem 4, \((g_0, \pi_0)\) agrees with a member of a chosen admissible family of asymptotically flat solutions ``near infinity" (i.e. outside a compact set). We note that while the energy-momentum can be made within \(\epsilon\) of the original value, this construction allows large changes in the angular momentum \(\mb{J}\) \cite{vacuum_asymptotics}.

In conclusion, we have that solutions of the vacuum Einstein constraint equations with special asymptotic properties are, in a certain sense, dense both among time-symmetric solutions (as shown in \cite{scalar_gluing}) and asymptotically flat solutions in general (as shown in \cite{vacuum_asymptotics}). Furthermore, there exist gluing constructions which allow connecting arbitrary asymptotically-flat solutions with a members of a family of asymptotically-flat solutions with the requisite flexibility \cite{vacuum_asymptotics}, generalizing a construction in \cite{scalar_gluing} for connecting time-symmetric, conformally flat solutions to members of the Schwarzschild family of solutions.

\printbibliography

\end{document}
