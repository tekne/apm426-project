# On the Asymptotics for the Vacuum Einstein Constraint Equations

## Terms
- [Kerr metric](https://en.wikipedia.org/wiki/Kerr_metric): geometry of empty spacetime around a rotating uncharged axially-symmetric black hole with a quasispherical event horizon.
- [Null infinity](https://en.wiktionary.org/wiki/null_infinity): in a closed space-time for a given point, the limit of convergence of the light cone most distant from the given point: the limit of a set of points that each have a time-like separation from every point that has a space-like separation from the given point.
- [Killing field](https://en.wikipedia.org/wiki/Killing_vector_field): a vector field on a Riemannian or pseudo-Riemannian (e.g. Lorentzian) manifold which preserves the metric. If nothing changes with time, the time vector will be a Killing vector (vector at a point in a Killing field), therefore the field will point in the direction of forward motion in time. A Killing field is determined uniquely by a vector at some point and its gradient (i.e. all covariant derivatives of the field at the point).

## Citations
- C: Corvino, `scalar_gluing.pdf`

## Summary
- Abstract: prove the set of asymptotically flat solutions with special asymptotics is dense in general classes of solutions of the vacuum constraint equations
  - First: harmonic asymptotics. Generalizes naturally to
  - Conformally flat asymptotics for the $`K = 0`$ constraint equations. We show:
  - Solutions with harmonic asymptotics form a dense subset (in a suitable weighted Sobolev topology) of the full set of solutions.
    - Important feature: approximation allows large changes in the angular momentum.
  - Second density theorem proved allows us to approximate asymptotically flat initial data on a three-manifold $`M`$ for the vacuum Einstein field equation by solutions which agree with the original data inside a given domain and are identical to that of a suitable Kerr slice or a member of some other admissible family of solutions outside a large ball in a given end. This construction generalizes work in which the time symmetric ($`K = 0`$ case was studied) in which case constraints reduce to equation $`R(g) = 0`$: zero scalar curvature. In `scalar_gluing` (C), we show how a Schwarzschild exterior can be selected and glued to AF time-symmetric data and compactly perturbed to a solution of the time-symmetric constraint. But in the non time-symmetric case, we have to consider the second fundamental form, and hence linear and angular momentum of the data at infinity. The Kerr solution (for example) is aptly suited for this.
  - In particular: show how to approximate given initial data for the Einstein vacuum equations by data which is exactly that of a space-like slice of a suitably chosen Kerr metric outside a compact set.
- Note: these constructions show a compact piece of the data dictates (in some sense) only part of the asymptotic structure: that part governed by energy-momentum and angular momentum. Having vacuum initial data identical to Kerr data outside a compact set is important for understanding the global evolution for the Einstein equations
  - In particular: such data evolves to produce a space-time with nice behavior at null infinity

- Two well known solutions of the Einstein equations:
  - Schwarzschild: rotational symmetry, static: outside the horizon have coordinates $`(t, x)`$ with $`r = |x|`$:
  $`g_S = -\left(1 - \frac{2m}{r}\right)dt^2 + \left(1 - \frac{2m}{r}\right)^{-1}dr^2 + r^2d\Omega^2`$.
  Static because $`\frac{\partial}{\partial t}`$ is a time-like Killing field orthogonal to the time-slices.
  - Kerr: axisymmetric and stationary: analogous Killing field is not orthogonal to the time slices.
- Interesting to us: these solutions are actually *families* of solutions to the Einstein equations.
  - Thinking of a fixed AF coordinate system near infinity in the space-time, by varying the total mass and angular momentum, and considering families of spacelike slices of these metrics, we get a ten-parameter family of AF solutions of the vacuum constraint equations near infinity in $`\mathbb{R}^3`$.
  - The purpose of considering this faimly is to control the energy-momentum $`(E, \mathbf{P})`$ and the angular momentum $`\mathbf{J}`$ at (space-like) infinity, as well as a quantity $`\mathbf{C}`$ related to the center-of-mass. This gives $`1 + 3 + 3 + 3 = 10`$ parameters (TODO: *check*)
  - In an asymptotically flat chart these quantities are defined as limits of integrals over Euclidean spheres with surface measure $`d\sigma_g`$ and outer normal $`\nu`$ taken with respect to $`g`$.
    - Nasty, nasty, nasty and *nasty*.
  - In the case where $`g`$ is AF, conformally flat near infinity and has zero scalar curvature, $`\mathbf{C}`$ is proportional to $`m\mathbf{c}_0`$ where $`m`$ is the mass and $`\mathbf{c}_0`$ is the coordinate translation which makes the $`|x|^{-2}`$-terms in the expansion of the conformal factor vanish, by C.
  - It may be useful to shift the quantity $`\mathbf{C}`$ (an initial shift in coordinates will do this), and also we note that the vector fields $`Y_i`$ are the basic rotation fields in $`\mathbb{R}^3`$.
  - The integrand for angular momentum is essentially the cross product of the position vector with the momentum tensor, in analogy with the angular momentum of classical mechanics.
  - We can prescribe conditions under which the above asymptotic integrals converge for *solutions* of the vacuum constraints: asymptotic even/odd conditions on the metric tensor and second fundamental form of the initial data. We call it (AC). The condition are
    - Nasty, nasty, nasty
  - Analogous conditions are required on successive derivatives as needed, implied in the big-O notation for the (AF) and (AC) conditions.
- Section 3: we show that the vacuum initial data sets which satisfy (AC) are dense in a suitable weighted Sobolev space in the set of all vacuum initial data which satisfy the standard decay assumptions (but not the asymptotic symmetry).
  - This space is strong enough that the total energy and linear momentum are continuous on the space. Of course, angular momentum is not continuous, nor does it appear to be well-defined on this more general space.
  - Theorem is an analogue for the full constraint equations of the theorem of (SY) in the time-symmetric case (K = 0). Much more subtle to prove though, as the general constraint operator is not as well understood as the time-symmetric operator.
  - We show: it is possible to achieve a very special type of asymptotic behavior which in particular satisfies (AC). These special asymptotic conditions require that outside a compact set there exist a positive function $`u`$ and a vector field $`X`$ so that $`g_{ij} = u^4\delta_{ij}`$ and
  $`\pi_{ij} = u^2\left(L_X\delta - \text{div}_\delta(X)\delta\right)_{ij}`$<!---_--> where $`L_X\delta`$ denotes the Lie derivative of the Euclidean metric $`\delta`$ with respect to $`X`$. It is not difficult to see that such data automatically satisfies (AC).
    - Important feature of these asymptotic conditions: total energy and momenta (both linear and angular) may be read off the asymptotics of $`(u, X)`$ and the behavior of these conserved quantities directly affects the asymptotic geometry.
  - Second density theorem (Theorem 5) will follow from a gluing construction (Theorem 4) coupled with the first density theorem. Proof for gluing is sections 4 and 5.
