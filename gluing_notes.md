# Gluing Initial Data Sets for General Relativity: Notes

## Citations
- [1]: `scalar_gluing.pdf`

## Summary
- In 1, we prove the existence of asymptotically flat, scalar flat metrics on \(\mathbb{R}^n\) for \(n \geq 3\) which are Schwarzschild outside of a compact set.
