# Notes on task

Papers to read:
- `scalar_gluing.pdf` (Corvino)
- `vacuum_asymptotics.pdf` (Corvino and Schoen)

Read the introductions o both papers, try to get the main idea as to why this problem is not trivial, what's important to understand, what's the idea behind it.
