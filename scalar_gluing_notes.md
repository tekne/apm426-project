# Scalar Deformation and a Gluing Construction for the Einstein Constraint Equation: Notes

Later:
- Yamabe problem solved by Trudinger, Aubin, and Schoen (who wrote the Vacuum Asymptotics paper): Every Riemannian metric on a closed manifold can be multiplied by some smooth positive function to obtain a metric with constant scalar curvature, i.e. every metric on a closed manifold is conformal to one with constant scalar curvature.

Terms:
- Scalar-flat: zero scalar curvature, but *check*.
- Cauchy data: initial data: 3D Riemannian manifold embedded into the 4D Lorentzian spacetime.
  - Cauchy surface: all past events influence what happens on the surface and all future events are influenced by what happens on.
- AF: asymptotically flat

- Paper goal: construct solutions to Einstein equations whose existence was previously an open question
  - Do there exist nontrivial metrics with zero scalar curvature in \(\mathbb{R}^n\) which are spherically symmetric (therefore Schwarzschild) outside a compact set ("at infinity")
  - This is false in punctured space, where nontrivial means non Schwarzschild.
  - If such metrics do not exist, then symmetry at infinity is a rigid condition, forcing symmetry everywhere
  - Bray determines the existence of nontrivial metrics with scalar curvature \(\geq 0\) which are Schwarzschild at infinity, used in his approach to the Penrose conjecture. But we need more analysis for the case of curvature exactly zero.
- Paper found:
  - Symmetry at infinity is *not* rigid, and in fact we can construct metrics which are Schwarzschild outside a compact set with considerable flexibility on the interior. By solving the Cauchy problem, we get small-time existence for spacetimes which are similarly nontrivial and Schwarzschild around spatial infinity.
  - In fact, the symmetric metrics are in some sense *generic* among AF scalar-flat metrics, in a way we will make precise below
- Method:
 - Glue an AF metric to a Schwarzschild metric at a "large" radius \(R\), with gluing occurring in an annular region in which the scalar curvature is near zero, decaying like a power of \(1/R\). Now, we want to set it to exactly zero without jeopardizing the symmetry at infinity.
 - Scalar curvature deformation on closed manifolds addressed by Fischer and Marsden: on a closed manifold the scalar curvature is generically a local surjection: given \(S\) which is the scalar curvature of a generic metric \(g\), any function sufficiently near \(S\) is also realizable as the scalar curvature of a metric near \(g\).
 - They do this near the flat metric on Euclidean space. We generalize to a compact domain in a Riemannian manifold \((M^n, g)\) for \(n \geq 2\), in the sense that given a sufficiently small compactly supported deformation of the scalar curvature we achieve it as the scalar curvature of a metric which differs from the initial metric only in a compact set.
- Outline:
 - Section 2: notation, preliminary statements, and the two main theorems to prove
 - Section 3: prove local scalar curvature deformation
 - Section 4: recall notion of asymptotically flat manifold, application of local curvature deformation to a conjecture, show minimal mass metrics (when they exist) are in fact static.
 - Section 5: existence of scalar flat metrics on \(\reals^n\) which are Schwarzschild at infinity. This is *not* just straightforward application of the local curvature deformation as an obstruction appears which precludes being able to simply deform scalar curvatures close to zero in the annulus to zero.
